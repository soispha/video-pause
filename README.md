# Video pause

An Add-on to automatically pause videos playing in the background

## Native messenger

A native messenger exits under the `nm` directory.
It's used to all currently running videos from the OS level.

## License

The whole project is licensed under the GPLv3-and-above license. See [Copying](./COPYING)
for more.

## Credit

The [Video pause icon was created by Freepik – Flaticon](https://www.flaticon.com/free-icons/pause).
