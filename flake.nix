{
  description = "An Add-on to automatically pause videos playing in the background";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/23.05";

    # inputs for following
    systems = {
      url = "github:nix-systems/x86_64-linux"; # only evaluate for this system
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs = {
        systems.follows = "systems";
      };
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    crane,
    rust-overlay,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [(import rust-overlay)];
      };

      nightly = false;
      rust =
        if nightly
        then pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default)
        else pkgs.rust-bin.stable.latest.default;

      rust-min =
        if nightly
        then pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.minimal)
        else pkgs.rust-bin.stable.latest.minimal;

      craneLib = (crane.mkLib pkgs).overrideToolchain rust-min;
      craneBuild = craneLib.buildPackage {
        src = craneLib.cleanCargoSource ./nm/.;
        doCheck = true;
      };
    in {
      devShells.default = pkgs.mkShell {
        packages = [
          pkgs.nodejs
          pkgs.nodePackages_latest.web-ext
          pkgs.nodePackages_latest.prettier

          rust
          pkgs.cargo-edit
          pkgs.cargo-expand

          pkgs.firefox
        ];
      };
      packages.default = craneBuild;
      app.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/video-pause";
      };
    });
}
# vim: ts=2

