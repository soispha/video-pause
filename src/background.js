// vim: ts=2
var playing_tab_id;

function disable_other_audible_tabs() {
  chrome.tabs.query({ active: true }, function (tabs) {
    var tab = tabs[0];
    chrome.tabs.sendMessage(
      tab.id,
      { message: "is_playing" },
      function (response) {
        if (!(response === undefined)) {
          if (response.playing) {
            if (
              !(playing_tab_id === undefined) &&
              !(playing_tab_id === tab.id)
            ) {
              chrome.tabs.sendMessage(playing_tab_id, { message: "pause" });
            }
            playing_tab_id = tab.id;
          }
        }
      }
    );
  });
}

function toggle_vod() {
  chrome.tabs.sendMessage(
    playing_tab_id,
    { message: "is_playing" },
    function (response) {
      if (!(response === undefined)) {
        if (response.playing) {
          chrome.tabs.sendMessage(playing_tab_id, { message: "pause" });
        } else {
          chrome.tabs.sendMessage(playing_tab_id, { message: "play" });
        }
      }
    }
  );
}

chrome.tabs.onUpdated.addListener(
  function (tabid, changeInfo, tab) {
    disable_other_audible_tabs();
  },
  { properties: ["audible"] }
);

// native massaging
var native_port = chrome.runtime.connectNative("video.pause");

native_port.onMessage.addListener(function (response) {
  switch (response.msg) {
    case "Got client":
    case "Finished setup!":
      // We simply ignore these for now
      break;
    case "toogle":
      toggle_vod();
      break;

    default:
      console.error("Native messanger: used unknow command: " + response.msg);
  }
});

native_port.onDisconnect.addListener(function (response) {
  console.error("Disconnected: " + response.error.message);
});
