// vim: ts=2
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  var video = document.getElementsByTagName("video")[0];
  switch (request.message) {
    case "pause":
      video.pause();
      break;

    case "play":
      video.play();
      break;

    case "is_playing":
      sendResponse({ playing: !video.paused });
      break;

    case "print":
      console.log(request.text);
      break;

    default:
      sendResponse({ playing: !video.paused });
  }
});
