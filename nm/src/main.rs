use anyhow::{Context, Result};
use clap::Parser;
use commands::{daemon::start_daemon, toggle::toggle};
use cli::{Args, Command};

mod cli;
mod commands;

fn main() -> Result<()> {
    let base_dir =
        xdg::BaseDirectories::with_prefix("video_pause").context("Failed to get basedir")?;
    let args = Args::parse();

    if let Some(command) = args.command {
        match command {
            Command::Toggle => toggle(base_dir)?,
            Command::Daemon => start_daemon(base_dir)?,
        };
    } else {
        start_daemon(base_dir)?;
    };
    Ok(())
}
