use clap::{Parser, command, Subcommand};

/// An native application for pausing videos in a Browser
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
#[command(next_line_help = true)]
pub struct Args {
    #[command(subcommand)]
    pub command: Option<Command>,
}

#[derive(Subcommand, Debug)]
pub enum Command {
    /// toggles the playback
    Toggle,
    /// starts the native-massaging host
    Daemon,
}
