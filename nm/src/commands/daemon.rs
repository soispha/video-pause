use std::{
    fs,
    io::{self, Read, Write},
    os::unix::net::{UnixListener, UnixStream},
    thread,
};

use anyhow::{Context, Result};
use byteorder::{NativeEndian, WriteBytesExt};
use xdg::BaseDirectories;

use super::consts::SOCKET_NAME;

pub fn start_daemon(xdg: BaseDirectories) -> Result<()> {
    let socket_path = xdg
        .place_runtime_file(SOCKET_NAME)
        .context("Unable to get XDG_RUNTIME_DIR")?;

    if socket_path.exists() {
        fs::remove_file(&socket_path)?;
    }
    let listener = UnixListener::bind(&socket_path)
        .with_context(|| format!("Can't bind to socket at {}", socket_path.display()))?;

    write_output("{\"msg\": \"Finished setup!\"}").unwrap();

    // accept connections and process them, spawning a new thread for each one
    loop {
        let (stream, _) = listener.accept()?;
        thread::spawn(|| handle_client(stream));
    }
}

pub fn handle_client(mut socket: UnixStream) -> Result<()> {
    write_output("{\"msg\":\"Got client\"}")?;
    let mut response = String::new();
    socket.read_to_string(&mut response)?;
    write_output(&response)?;
    Ok(())
}

// source: https://gist.github.com/TotallyNotChase/c747c55d4a965954f49a7fa5c3f344e0/
// https://dev.to/totally_chase/pushing-native-messaging-to-the-limits-c-vs-rust-4nad?comments_sort=top
pub fn write_output(msg: &str) -> Result<()> {
    let mut output = io::stdout();
    let len = msg.len();
    // Chrome won't accept a message larger than 1MB
    if len > 1024 * 1024 {
        panic!("Message was too large, length: {}", len)
    }
    output.write_u32::<NativeEndian>(len as u32)?;
    output.write_all(msg.as_bytes())?;
    output.flush()?;
    Ok(())
}
