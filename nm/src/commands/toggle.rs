use std::{io::Write, os::unix::net::UnixStream};

use anyhow::{bail, Context, Result};
use xdg::BaseDirectories;

use super::consts::SOCKET_NAME;

pub fn toggle(xdg: BaseDirectories) -> Result<()> {
    let socket_path = match xdg.find_runtime_file(SOCKET_NAME) {
        Some(path) => path,
        None => bail!(
            "No daemon socket in runtime dir with name: '{}', is a daemon running?",
            SOCKET_NAME
        ),
    };
    let mut socket = UnixStream::connect(&socket_path).context(format!(
        "Trying to connect to socket at path ('{}')",
        socket_path.display()
    ))?;
    socket
        .write(b"{\"msg\": \"toggle\"}") // msg is checked in the background.js file
        .context("Trying to write to socket")?;
    Ok(())
}
